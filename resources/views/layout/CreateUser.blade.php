@extends('master')
@section('content')
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-lg-12 col-xl-11">
          <div class="card text-black" style="border-radius: 25px;">
            <div class="card-body p-md-5">
              <div class="row justify-content-center">
                <div class="col-md-10 col-lg-6 col-xl-5 order-2 order-lg-1">
                  <p class="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">Sign up</p>

                    @if (Session::has('thanhcong'))
                        <h2 style="color: blue">{{ Session::get('thanhcong') }}</h2>
                    @endif
                  <form class="mx-1 mx-md-4" action="{{ route('register') }}" method="post">
                    @csrf
                    <div class="d-flex flex-row align-items-center mb-4">
                      <div class="form-outline flex-fill mb-0">
                        <input type="text" id="form3Example1c" name="name" value="{{ old('name') }}"  class="form-control" placeholder="Name"  />
                      </div>
                        <span style="color: red">
                            @error('name')
                                {{ $message }}
                            @enderror
                        </span>
                    </div>

                    <div class="d-flex flex-row align-items-center mb-4">
                      <div class="form-outline flex-fill mb-0">
                        <input type="email" id="form3Example3c" value="{{ old('email') }}" name="email" class="form-control" placeholder="Email"  />
                      </div>
                      <span style="color: red">
                        @error('email')
                            {{ $message }}
                        @enderror
                    </span>
                    </div>

                    <div class="d-flex flex-row align-items-center mb-4">
                      <div class="form-outline flex-fill mb-0">
                        <input type="password" id="form3Example4c"  name="password" class="form-control" placeholder="Password"   />
                      </div>
                      <span style="color: red">
                        @error('password')
                            {{ $message }}
                        @enderror
                    </span>
                    </div>

                    <div class="d-flex flex-row align-items-center mb-4">
                      <div class="form-outline flex-fill mb-0">
                        <input type="password" id="form3Example4cd" name="repeatpassword" class="form-control" placeholder="Repeat Password"  />
                      </div>
                      <span style="color: red">
                        @error('repeatpassword')
                            {{ $message }}
                        @enderror
                    </span>
                    </div>

                    <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                      <button type="submit" class="btn btn-primary btn-lg">Register</button>
                    </div>

                  </form>

                </div>
                <div class="col-md-10 col-lg-6 col-xl-7 d-flex align-items-center order-1 order-lg-2">

                  <img src="https://mdbootstrap.com/img/Photos/new-templates/bootstrap-registration/draw1.png" class="img-fluid" alt="Sample image">

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

@endsection
