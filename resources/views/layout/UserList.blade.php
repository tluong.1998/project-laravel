@extends('master')
@section('content')
  <div class="page-header">
      <h1>User - ToDo List </h1>
  </div>
  <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
          <div class="input-group">
              <input type="text" class="form-control" placeholder="Search item name" />

          </div>
      </div>
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4">
          <a href="{{ route('CreateUser') }}" class="btn btn-info btn-block ">Add Item</a>
      </div>
  </div>
  {{-- <div class="row marginB10">
      <div class="col-md-offset-7 col-md-5">
          <form class="form-inline">
              <div class="form-group">
                  <input type="text" class="form-control" placeholder="Item Name" />
              </div>
              <div class="form-group">
                  <select class="form-control">
                      <option value="0">Small</option>
                      <option value="1">Medium</option>
                      <option value="2">High</option>
                  </select>
              </div>
              <button type="button" class="btn btn-primary">Submit</button>
              <button type="button" class="btn btn-default">Cancel</button>
          </form>
      </div>
  </div> --}}
  <div class="list-panel panel panel-success">
      <div class="panel-heading">List Item</div>
      <table class="table table-hover ">
          <thead>
              <tr>
                  <th style="width: 10%" class="text-center">Id</th>
                  <th style="width: 10%" class="text-center">Name</th>
                  <th style="width: 15%" class="text-center">Address</th>
                  <th style="width: 15%" class="text-center">Phone</th>
                  <th style="width: 15%" class="text-center">Email</th>
                  <th style="width: 15%" class="text-center">Password</th>
                  <th style="width: 15%">Action</th>
              </tr>
          </thead>
          <tbody>
              <tr>
                  <td class="text-center">1</td>
                  <td class="text-center">Tìm thấy mảnh</td>
                  <td class="text-center">nghe an</td>
                  <td class="text-center">0232133123</td>
                  <td class="text-center">luong@gmail.com</td>
                  <td class="text-center">************</td>
                  <td>
                      <button type="button" class="btn btn-edit btn-warning btn-sm">Edit</button>
                      <button type="button" class="btn btn-danger btn-sm">Delete</button>
                  </td>
                  <tr>
                    <td class="text-center">1</td>
                    <td class="text-center">Tìm thấy mảnh</td>
                    <td class="text-center">nghe an</td>
                    <td class="text-center">0232133123</td>
                    <td class="text-center">luong@gmail.com</td>
                    <td class="text-center">************</td>
                    <td>
                        <button type="button" class="btn btn-edit btn-warning btn-sm">Edit</button>
                        <button type="button" class="btn btn-danger btn-sm">Delete</button>
                    </td>
                    <tr>
                        <td class="text-center">1</td>
                        <td class="text-center">Tìm thấy mảnh</td>
                        <td class="text-center">nghe an</td>
                        <td class="text-center">0232133123</td>
                        <td class="text-center">luong@gmail.com</td>
                        <td class="text-center">************</td>
                        <td>
                            <button type="button" class="btn btn-edit btn-warning btn-sm">Edit</button>
                            <button type="button" class="btn btn-danger btn-sm">Delete</button>
                        </td>
                        <tr>
                            <td class="text-center">1</td>
                            <td class="text-center">Tìm thấy mảnh</td>
                            <td class="text-center">nghe an</td>
                            <td class="text-center">0232133123</td>
                            <td class="text-center">luong@gmail.com</td>
                            <td class="text-center">************</td>
                            <td>
                                <button type="button" class="btn btn-edit btn-warning btn-sm">Edit</button>
                                <button type="button" class="btn btn-danger btn-sm">Delete</button>
                            </td>
                            <tr>
                                <td class="text-center">1</td>
                                <td class="text-center">Tìm thấy mảnh</td>
                                <td class="text-center">nghe an</td>
                                <td class="text-center">0232133123</td>
                                <td class="text-center">luong@gmail.com</td>
                                <td class="text-center">************</td>
                                <td>
                                    <button type="button" class="btn btn-edit btn-warning btn-sm">Edit</button>
                                    <button type="button" class="btn btn-danger btn-sm">Delete</button>
                                </td>

              {{-- <tr>
                  <td class="text-center">6</td>
                  <td><input type="text" class="form-control" value="F1 muốn tổ chức giải đua xe tại Việt Nam vào năm 2020"></td>
                  <td class="text-center">
                    <select class="form-control">
                      <option>Small</option>
                      <option>Medium</option>
                      <option>High</option>
                    </select>
                  </td>
                  <td>
                      <button type="button" class="btn btn-default btn-sm">Cancel</button>
                      <button type="button" class="btn btn-success btn-sm">Save</button>
                  </td>
              </tr> --}}

          </tbody>
      </table>
  </div>

@endsection
