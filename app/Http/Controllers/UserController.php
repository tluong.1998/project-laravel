<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Auth\Events\Validated;
use Illuminate\Support\Facades\Hash;
use Session;

session_start();

class UserController extends Controller
{
    //
    // function register(Request $req)
    // {
    //     // $user = new User;
    //     // $user->name= $req->input('name');
    //     // $user->email= $req->input('email');
    //     // $user->password= Hash::make($req->input('password'));
    //     // $user->save();
    //     dd($req->all());
    //     // return  "hello";
    // }

    public function create()
    {
        return view('layout.CreateUser');
    }

    public function createUser()
    {
        return view('layout.create');
    }

    public function register(Request $request)
    {
        $request->validate(
            [
                'name' => ' required | alpha | min:6 ',
                'email' => 'required | email',
                'password' => 'required | min:6',
                'repeatpassword' => 'required | same:password'
            ],
            [
                'name.required' => 'Tên không được để trống',
                'name.alpha' => 'Không được điền các kí tự số',
                'name.min' => 'Tên phải lớn hơn 6 kí tự',
                'email.required' => 'Email không được để trống',
                'password.required' => 'Mật khẩu không được để trống',
                'password.min' => 'Mật khẩu phải lớn hơn 6 kí tự',
                'repeatpassword.required' => 'Mật khẩu không được để trống',
                'repeatpassword.same' => 'Mật khẩu không khớp'
            ]
        );

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->name);
        $user->save();

        return redirect()->back()->with('thanhcong', 'Tạo tài khoản thành công');
    }

    public function post(Request $request)
    {
        dd($request->all());
    }
}
