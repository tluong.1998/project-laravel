<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout.dashboard');
})->name('home');

Route::get('dashboard', function () {
    return view(('layout.dashboard'));
})->name('dashboard');

Route::get('userlist', function () {
    return view('layout.UserList');
})->name('UserList');

Route::get('create', [UserController::class, 'create'])->name('CreateUser');

Route::post('register', [UserController::class, 'register'])->name('register');

// Route::get('createUser', [UserController::class, 'createUser'])->name('createUser');

// Route::post('postUser', [UserController::class, 'post'])->name('postUser');
